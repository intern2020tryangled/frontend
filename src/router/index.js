import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/login'
import Patient from '@/components/patient'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: "/login",
            redirect: {
                name: "Login",
                component: Login

            }
        },
        {
            path: '/patient',
            name: 'Patient',
            component: Patient
        }
    ]
})



