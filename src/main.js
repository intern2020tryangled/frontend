import Vue from 'vue'
import App from './App.vue'
import responsive from 'vue-responsive'
import BootstrapVue from 'bootstrap-vue'
import ToggleButton from 'vue-js-toggle-button'
import VueRouter from 'vue-router'
import routes from './routes';


import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



Vue.use(BootstrapVue);
Vue.use(ToggleButton);
Vue.use(responsive);
Vue.use(VueRouter);

const router = new VueRouter({routes});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')



