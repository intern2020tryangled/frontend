import login from './components/login.vue';
import patient from './components/patient.vue';
import partner from './components/partner.vue';
import task from './components/task.vue';
import summery from './components/summery.vue';
import settings from './components/settings.vue';


const routes = [
    { path: '/login', component: login },
    { path: '/patient', component: patient },
    { path: '/partner', component: partner },
    { path: '/task', component: task },
    { path: '/summery', component: summery },
    { path: '/settings', component: settings },

];

export default routes;